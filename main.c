#include <stdio.h>
#include "ttt.h"

char playerToChar(Player p) {
	switch (p) {
		case NONE:
			return ' ';
		case X:
			return 'X';
		case O:
			return 'O';
	}
}

Coords digitToCoords(int d) {
	Coords c;
	c.m = (d - 1) / 3;
	c.n = (d - 1) % 3;
	return c;
}

void print(Game g) {
	for (int m = 0; m < 3; m++) {
		for (int n = 0; n < 3; n++) {
			printf("%c ", playerToChar(g.grid[m][n]));
		}
		printf("\n");
	}
}

int main(int argc, char **argv) {
	Game game;
	initGame(&game);

	for (;;) {
		int input;
		Coords c;
		do {
			scanf("%d", &input);
			c = digitToCoords(input);
		} while (place(&game, game.player, c));

		print(game);

		if (won(game, game.player))
			break;
		game.player = invPlayer(game.player);
	}

	return 0;
}
