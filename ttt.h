#ifndef TTT
#define TTT

typedef unsigned int uint;

typedef enum {
	NONE, X, O
} Player;

typedef struct {
	int m;
	int n;
} Coords;

typedef struct {
	Player grid[3][3];
	Player player;
	uint scoreX;
	uint scoreO;
} Game;

Player invPlayer(Player);
void initGame(Game*);
void resetGame(Game*);
int place(Game*, Player, Coords);
int won(Game, Player);

#endif
