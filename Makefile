CC=gcc
CFLAGS=-Wall
LDFLAGS=
EXEC=ttt

all: $(EXEC)

ttt: ttt.o main.o
	$(CC) -o ttt ttt.o main.o $(LDFLAGS)

ttt.o: ttt.c
	$(CC) -o ttt.o -c ttt.c $(CFLAGS)

main.o: main.c ttt.h
	$(CC) -o main.o -c main.c $(CFLAGS)

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)
