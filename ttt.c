#include "ttt.h"

Player invPlayer(Player p) {
	switch (p) {
		case X: return O;
		case O: return X;
	}
	return NONE;
}

void initGame(Game *g) {
	resetGame(g);
	g->player = X;

	g->scoreX = 0;
	g->scoreO = 0;
}

void resetGame(Game *g) {
	for (int m = 0; m < 3; m++) {
		for (int n = 0; n < 3; n++) {
			g->grid[m][n] = NONE;
		}
	}
}

int place(Game *g, Player p, Coords c) {
	if (g->grid[c.m][c.n] == NONE) {
		g->grid[c.m][c.n] = p;
		return 0;
	}

	/* We can't place the player here */
	return 1;
}

int won(Game g, Player p) {
	for (int i = 0; i < 3; i++) {
		/* Line */
		if (g.grid[i][0] == p
		 && g.grid[i][1] == p
		 && g.grid[i][2] == p)
			return 1;

		/* Column */
		if (g.grid[0][i] == p
		 && g.grid[1][i] == p
		 && g.grid[2][i] == p)
			return 1;
	}

	/* Diagonals */
	if (g.grid[0][0] == p
	 && g.grid[1][1] == p
	 && g.grid[2][2] == p)
		return 1;

	if (g.grid[0][2] == p
	 && g.grid[1][1] == p
	 && g.grid[2][0] == p)
		return 1;

	return 0;
}
